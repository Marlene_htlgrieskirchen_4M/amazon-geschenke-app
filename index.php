<!DOCTYPE html>
<html>
    <head>
            <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro&display=swap" rel="stylesheet">
        <meta charset="utf-8">
        <title>Amazing Wish</title>
        <link rel="stylesheet" href="css/stylesheets.css">

    </head>
    <body>

        <div class="container">
    <div class="main-header-container">

        <header class="main-header clearfix">

            <a class="logo" href="index.php">
                <img src="img/logo.PNG" height="50">
            </a>
            <nav>
                <ul class="clearfix">

                    <li><a href="/backend/index.php">Login</a></li>

                </ul>

            </nav>

        </header>
    </div>
    <div class="intro-container">
        <section class="intro">
            <h1>Amazing Wish <span>by Marlene und Amelie</span></h1>
            <img src="img/baum.png">
        </section>
    </div>

        </div>

    </body>
</html>
